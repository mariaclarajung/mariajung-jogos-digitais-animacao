﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScript : MonoBehaviour
{
    private float screenWidh;
    void Start()
    {
        // Permite mexer na imagem como você quiser (flip, tamanho...)
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();

        // Pega o tamanho da imagem 
        float imageWidh = renderer.sprite.bounds.size.x;
        float imageHeight = renderer.sprite.bounds.size.y;

        // Pega o tamanho da tela
        float screenHeight = Camera.main.orthographicSize*2;
        screenWidh = screenHeight / Screen.height * Screen.width;

        // DANDO A NOVA ESCALA DA IMAGEM DE ACORDO COM O TAMANHO DA TELA

        // Variável que vai pegar o tamanho da imagem
        Vector2 newScale = this.transform.localScale;

        // Reescala o tamanho da imagem para o tamanho da tela
        newScale.x = screenWidh / imageWidh + 0.25f;
        newScale.y = screenHeight / imageHeight;

        // Exibe o tamanho novo da imagem
        this.transform.localScale = newScale;
    }

    void Update()
    {

    }
}
