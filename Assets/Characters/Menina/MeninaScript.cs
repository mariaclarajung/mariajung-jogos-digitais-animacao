﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeninaScript : MonoBehaviour
{
    // Permete controlar a velocidade do andar na plataforma
    public float velocity;
    // Variáveis body e render carregadas aqui para poder usar elas tanto no Start quanto no Update
    private Rigidbody2D body; 
    private SpriteRenderer render;
    // Variável que vai me pemitir parar ou rodar o "GIF" do meu personagem
    private Animator animator;

    void Start()
    {
        this.body = this.GetComponent<Rigidbody2D>();
        this.render = this.GetComponent<SpriteRenderer>();
        this.animator = this.GetComponent<Animator>();

        // Diz que no início do jogo o GIF do personagem etará false, ou seja, parado
        this.animator.enabled = false;
    }

    void Update()
    {
        // Variáveis que pegam a posição de x e y do meu personagem
        float x = this.transform.position.x;
        float y = this.transform.position.y;

        // Código que permite:
        // - o movimento horizontal do personagem
        // - fazer com que o personagem vire para o lado que estiver caminhando
        // - pare com mais precisão quando as setas do teclado não estiverem sendo pressionadas
        // - torne o animator true ou false de acordo com o status de movimentação do personagem
        float inputKeyHorizontal = Input.GetAxis("Horizontal");

        if (inputKeyHorizontal > 0)
        {
            this.body.AddForce(new Vector2(this.velocity, 0));
            this.render.flipX = false;
            this.animator.enabled = true;
        }
        else if(inputKeyHorizontal < 0)
        {
            this.body.AddForce(new Vector2(-this.velocity, 0));
            this.render.flipX = true;
            this.animator.enabled = true;
        }
        else
        {
            this.body.MovePosition(new Vector2(x, y));
            this.animator.enabled = false;
        }
    }
}
